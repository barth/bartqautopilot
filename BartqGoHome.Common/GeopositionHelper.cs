﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif

{
    public class GeopositionHelper
    {
        private static Geolocator geolocator = null;

        public static Geolocator Geolocator
        {
            get
            {
                if (geolocator == null)
                    geolocator = new Geolocator()
                    {
                        DesiredAccuracy = PositionAccuracy.High
                    };
                return geolocator;
            }
        }

        public static async Task<GeoCoordinate> GetCurrentLocation()
        {
            try
            {
                Geoposition geoposition = await Geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(10)
                    );

                return new GeoCoordinate(geoposition.Coordinate.Point.Position.Latitude, geoposition.Coordinate.Point.Position.Longitude); ;
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == 0x80004004)
                {
                    // the application does not have the right capability or the location master switch is off
                    throw new PlatformNotSupportedException("Pobieranie lokacji zostało wyłączone w ustawieniach telefonu.", ex);
                }
                //else
                {
                    throw new NotSupportedException("Telefon nie chce współpracować, Twoja pozycja pozostaje nieznana. " +
                        " Spróbuj później, upewniając się, że masz połączenie z internetem i sygnał GPS.", ex);
                }
            }
        }
    }
}
