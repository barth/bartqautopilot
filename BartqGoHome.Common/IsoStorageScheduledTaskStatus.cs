﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif

{
    public class IsoStorageScheduledTaskStatus : IsoStoreMutexedFile
    {
        private static IsoStorageScheduledTaskStatus instance;
        public static IsoStorageScheduledTaskStatus Instance
        {
            get
            {
                if (instance == null)
                    instance = new IsoStorageScheduledTaskStatus() { FileName = "ScheduledTask.log" };
                return instance;
            }
        }

        public async Task Write(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            while (e != null)
            {
                sb.AppendLine(e.Message);
#if DEBUG
            sb.AppendLine(e.StackTrace);
#endif
                e = e.InnerException;
            }
            await Write("error|" + sb.ToString());
        }

        public async Task WriteError(string msg)
        {
            await Write("error|" + msg);
        }

        public override async Task Write(string msg)
        {
            msg = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "|" + msg;
            await base.Write(msg);
        }

        public override async Task<string> Read()
        {
            string msg = await base.Read();
            if (string.IsNullOrWhiteSpace(msg))
                msg = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "error|Brak wiadomości";
            return msg;
        }
    }
}
