﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif
{
    public class IsoStorageSettings : IsoStoreMutexedFile
    {
        private static IsoStorageSettings instance;
        public static IsoStorageSettings Instance
        {
            get
            {
                if (instance == null)
                    instance = new IsoStorageSettings() { FileName = "ScheduledTask.settings" };
                return instance;
            }
        }

        public async Task Write(params string[] @params)
        {
            if (@params.All(p => p != null))
            {
                var msg = @params.Aggregate((acc, p) => acc.Length == 0 ? p : acc + "|" + p);
                await base.Write(msg);
            }
        }

        public new async Task<string[]> Read()
        {
            string msg = await base.Read();
            string[] @params = msg.Split('|');
            return @params;
        }

        public bool Exists
        {
            get
            {
                return isostore.FileExists(FileName);
            }
        }
    }
}
