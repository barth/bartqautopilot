﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif

{
    public class IsoStoreMutexedFile
    {
        public string FileName { get; set; }
        private Mutex mutex;
        protected static readonly IsolatedStorageFile isostore = IsolatedStorageFile.GetUserStoreForApplication();

        public virtual async Task<string> Read()
        {
            string msg = string.Empty;
            mutex = new Mutex(false, FileName);
            try
            {
                mutex.WaitOne();
                using (StreamReader sw = new StreamReader(new IsolatedStorageFileStream(FileName, FileMode.Open, FileAccess.Read, isostore)))
                {
                    msg = await sw.ReadToEndAsync();
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
            }
            finally
            {
                try
                {
                    mutex.ReleaseMutex();
                }
                catch { }
                finally
                {
                    mutex.Dispose();
                }
            }
            return msg;
        }

        public virtual async Task Write(string msg)
        {
            mutex = new Mutex(false, FileName);
            try
            {
                mutex.WaitOne();
                using (StreamWriter sw = new StreamWriter(
                    new IsolatedStorageFileStream(FileName, FileMode.Create, FileAccess.Write, isostore)))
                {
                    await sw.WriteAsync(msg);
                }
            }
            catch
            {
            }
            finally
            {
                try
                {
                    mutex.ReleaseMutex();
                }
                catch { }
                finally
                {
                    mutex.Dispose();
                }
            }
        }
    }

}
