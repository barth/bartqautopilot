﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif

{
    public class JakDojadeService
    {
        private const double MINDISTANCE = 0.003;//ok 100m.

        public static async System.Threading.Tasks.Task<string> GetUrl(GeoCoordinate currentPosition, string tolat, string tolon, string cid, string toaddress)
        {
            CultureInfo ci = new CultureInfo("En-us");
            GeoCoordinate coordinate = null;
            if (currentPosition != null)
                coordinate = currentPosition;
            else
                coordinate = await GeopositionHelper.GetCurrentLocation();

            if (AtHome(coordinate, tolat, tolon))
                return null;

            var currlatitude = (coordinate.Latitude).ToString("###.######", ci);
            var currlongitude = (coordinate.Longitude).ToString("###.######", ci);

            var from = string.Format("{0}:{1}", currlatitude, currlongitude);

            var to = string.Empty;


            if (string.IsNullOrWhiteSpace(tolat) || string.IsNullOrWhiteSpace(tolon))
            {
                to = string.Format(ci, "tn={0}", Uri.EscapeUriString(toaddress));
            }
            else
            {
                var dlat = double.Parse(tolat);
                var dlon = double.Parse(tolon);
                to = string.Format(ci, "tc={0:###.######}:{1:###.######}", dlat, dlon);
            }

            string url = "http://m.jakdojade.pl/?"+string.Format("fc={0}&{1}&td=domek&cid={2}&computeTrip=true", from, to, cid);
            return url;
        }

        private static bool AtHome(GeoCoordinate coordinate, string tolat, string tolon)
        {
            var tolatl = double.Parse(tolat);
            var tolonl = double.Parse(tolon);

            var dist = tolatl - coordinate.Latitude;
            dist = dist * dist;

            var dist2 = tolonl - coordinate.Longitude;
            dist2 = dist2 * dist2;

            return Math.Sqrt(dist+dist2)<MINDISTANCE;
        }

    }
}
