﻿using Microsoft.Phone.Info;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BartqGoHome.Common
{

    public class LittleWatson
    {
        const string filename = "LittleWatson.txt";

        public static void ReportException(Exception ex, string extra)
        {
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    SafeDeleteFile(store);
                    using (TextWriter output = new StreamWriter(store.CreateFile(filename)))
                    {
                        output.WriteLine(extra);
                        output.WriteLine(ex.Message);
                        output.WriteLine(ex.StackTrace);
                        output.WriteLine("----------------------------");
                        output.WriteLine("Device name: "+Microsoft.Phone.Info.DeviceStatus.DeviceName);
                        output.WriteLine("Manufacturer: "+DeviceStatus.DeviceManufacturer);
                        output.WriteLine("OS version: " + System.Environment.OSVersion);
                        output.WriteLine("Firmware version: "+DeviceStatus.DeviceFirmwareVersion);
                        output.WriteLine("Hardware version: " + DeviceStatus.DeviceHardwareVersion);
                        output.WriteLine("Device total memory: " + ToHumanFormat(DeviceStatus.DeviceTotalMemory));
                        output.WriteLine("Current memory usage: "+ToHumanFormat(DeviceStatus.ApplicationCurrentMemoryUsage));
                        output.WriteLine("Application memory usage limit: "+ToHumanFormat(DeviceStatus.ApplicationMemoryUsageLimit));
                        output.WriteLine("Application peak memory usage: "+ToHumanFormat(DeviceStatus.ApplicationPeakMemoryUsage));
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private static string ToHumanFormat(long len)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            string result = String.Format("{0:0.##}{1}", len, sizes[order]);
            return result;
        }

        public static void CheckForPreviousException()
        {
            try
            {
                string contents = null;
                string extra = null;
                string message = null;
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists(filename))
                    {
                        using (TextReader reader = new StreamReader(store.OpenFile(filename, FileMode.Open, FileAccess.Read, FileShare.None)))
                        {
                            extra = reader.ReadLine();
                            message = reader.ReadLine();
                            contents = reader.ReadToEnd();
                        }
                        SafeDeleteFile(store);
                    }
                }
                if (!string.IsNullOrWhiteSpace(contents) || !string.IsNullOrWhiteSpace(message) ||
                    !string.IsNullOrWhiteSpace(extra))
                {
                    if (MessageBox.Show("Przy ostatnim uruchomieniu aplikacja wykrzaczyła się żałośnie. Czy chcesz zawstydzić autora?", "Wyślij email z raportem o błędzie.", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        EmailComposeTask email = new EmailComposeTask();
                        email.To = "bci@poczta.onet.pl";
                        email.Subject = "[BartqAutopilot auto-generated problem report] "+message;
                        email.Body = contents;
                        SafeDeleteFile(IsolatedStorageFile.GetUserStoreForApplication());
                        email.Show();
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                SafeDeleteFile(IsolatedStorageFile.GetUserStoreForApplication());
            }
        }

        private static void SafeDeleteFile(IsolatedStorageFile store)
        {
            try
            {
                store.DeleteFile(filename);
            }
            catch (Exception)
            {
            }
        }
    }

}
