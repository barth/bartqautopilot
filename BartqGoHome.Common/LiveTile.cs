﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif

{
    public class MyLiveTile
    {
        public static IEnumerable<ShellTile> GetTiles()
        {
            return ShellTile.ActiveTiles.ToList();
        }
#if !BACKGROUND_AGENT
        public static void CreateLiveTile(int mode)
        {
            try
            {
                string paramname, desc;
                ModeToParams(mode, out paramname, out desc);
                Uri tileUri = new Uri("/Pages/MainPage.xaml?tile=" + paramname, UriKind.Relative);
                ShellTileData tileData = CreateFlipTileData(paramname, desc, desc, mode);
                ShellTile.Create(tileUri, tileData, true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Tworzenie kafelka nie powiodło się." + Environment.NewLine + e.Message);
            }
        }
#endif
        private static void ModeToParams(int mode, out string paramname, out string desc)
        {
            switch (mode)
            {
                case 0:
                    paramname = "Bus";
                    desc = "kom. miejska";
                    break;
                case 1:
                    paramname = "Car";
                    desc = "samochód";
                    break;
                case 2:
                    paramname = "Walk";
                    desc = "pieszo";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static FlipTileData CreateFlipTileData(string filename, string desc, string longdesc, int mode, bool nobackbackground = false)
        {
            FlipTileData flipTile = new FlipTileData();
            flipTile.Title = "Bartq Autopilot";
            flipTile.BackTitle = "Bartq Autopilot";

            flipTile.BackContent = desc;

            flipTile.BackgroundImage = new Uri("/Assets/Tiles/FlipTile" + filename + "Medium.png", UriKind.Relative); //Default image for Background Image Medium Tile 336x336 px
            //End Medium size Tile 336x336 px
            flipTile.WideBackgroundImage = new Uri("/Assets/Tiles/FlipTile" + filename + "Large.png", UriKind.Relative);

            flipTile.WideBackContent = longdesc;

            //Medium size Tile 336x336 px            
            //Crete image for BackBackgroundImage in IsoStore
            if (!nobackbackground)
            {
                flipTile.BackBackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileMedium.png",
                            UriKind.Relative); //Generated image for Back Background 336x336

                flipTile.WideBackBackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileWide.png",
                            UriKind.Relative); //Generated image for Back Background 336x336
            }
            else
            {
                flipTile.BackBackgroundImage = new Uri("/Assets/Tiles/FlipCycleEmptyTileMedium.png",
                            UriKind.Relative);
                flipTile.WideBackBackgroundImage = new Uri("/Assets/Tiles/FlipCycleEmptyTileLarge.png",
                            UriKind.Relative); 
            }

            //End Wide size Tile 691x336 px
            return flipTile;
        }

        public static FlipTileData CreateFlipTileData(List<Route> routes)
        {
            routes.Reverse();
            var route = routes.FirstOrDefault();
            if (route != null)
            {
                string desc = RouteToString(route);

                string longdesc = desc;

                route = routes.Skip(1).FirstOrDefault();

                if (route != null)
                    longdesc = longdesc + Environment.NewLine + RouteToString(route);

                FlipTileData flipTile = CreateFlipTileData("Bus", desc,longdesc, 0, true);
                return flipTile;
            }
            return null;
        }

        private static string RouteToString(Route route)
        {
            string desc = route.Lines;
            if (!string.IsNullOrWhiteSpace(route.LeavingAt))
                desc += " wyjdź " + route.LeavingAt;
            if (!string.IsNullOrWhiteSpace(route.StartAt))
                desc += Environment.NewLine + route.StartAt;
            if (!string.IsNullOrWhiteSpace(route.ArriveAt))
                desc += " - " + route.ArriveAt;
            return desc;
        }

        private static void RenderText(string text, int width, int height, int fontsize, string imagename)
        {
            WriteableBitmap b = new WriteableBitmap(width, height);

            var canvas = new Grid();
            canvas.Width = b.PixelWidth;
            canvas.Height = b.PixelHeight;

            var background = new Canvas();
            background.Height = b.PixelHeight;
            background.Width = b.PixelWidth;

            //Created background color as Accent color
            SolidColorBrush backColor = new SolidColorBrush((Color)Application.Current.Resources["PhoneAccentColor"]);
            background.Background = backColor;

            var textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.FontWeight = FontWeights.Bold;
            textBlock.TextAlignment = TextAlignment.Left;
            textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            textBlock.VerticalAlignment = VerticalAlignment.Stretch;
            textBlock.Margin = new Thickness(35);
            textBlock.Width = b.PixelWidth - textBlock.Margin.Left * 2;
            textBlock.TextWrapping = TextWrapping.Wrap;
            textBlock.Foreground = new SolidColorBrush(Colors.White); //color of the text on the Tile
            textBlock.FontSize = fontsize;

            canvas.Children.Add(textBlock);

            b.Render(background, null);
            b.Render(canvas, null);
            b.Invalidate(); //Draw bitmap

            //Save bitmap as jpeg file in Isolated Storage
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream imageStream = new IsolatedStorageFileStream(imagename, System.IO.FileMode.Create, isf))
                {
                    b.SaveJpeg(imageStream, b.PixelWidth, b.PixelHeight, 0, 100);
                }
            }
        }
    }
}
