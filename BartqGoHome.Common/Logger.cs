﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqGoHome.Common
{
    public class Logger
    {

        public Logger()
        {
            EnabledTags = new HashSet<string>();
        }

        public Logger(string tags) : this()
        {
            AppendTags(tags);
        }

        public const string FileName = "log.txt";

        HashSet<string> EnabledTags;

        public void Log(string tags, string message, params object[] args)
        {          
            try
            {
                HashSet<string> tgs = ParseTags(tags);
                
                if(!EnabledTags.Overlaps(tgs)) return;

                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using(var stream = store.OpenFile(FileName, FileMode.Append, 
                        FileAccess.Write, FileShare.Read | FileShare.Delete)) {
                        using (StreamWriter sw = new StreamWriter(stream)) {
                            var formattedMessage = string.Format(message,args);
                            sw.WriteLine(string.Format("{0} [{1}] {2}",DateTime.Now,tags,formattedMessage));
                        }
                    }
                }
            }
            catch {

            }
        }

        public void Log(string tags, Exception e)
        {
            do
            {
                Log(tags, e.Message);
                Log(tags, e.StackTrace);
                e = e.InnerException;
            }
            while (e != null);
        }

        private HashSet<string> ParseTags(string tags)
        {
            return new HashSet<string>(tags.ToLower().Split(' '));
        }

        public void AppendTags(string tags)
        {
            var tset = ParseTags(tags);
            EnabledTags.UnionWith(tset);
        }

        public void ClearTags()
        {
            EnabledTags.Clear();
        }

        public void Clear()
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if(store.FileExists(FileName))
                    store.DeleteFile(FileName);
            }
        }

        public static bool Present
        {
            get
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    return store.FileExists(FileName);
                }
            }
        }
    }
}
