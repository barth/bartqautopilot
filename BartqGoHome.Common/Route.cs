﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BACKGROUND_AGENT
namespace BartqGoHome.SchedulerTaskAgent.Common
#else
namespace BartqGoHome.Common
#endif

{
    public class Route
    {
        public string Lines { get; set; }
        public string LeavingAt { get; set; }
        public string StartAt { get; set; }
        public string ArriveAt { get; set; }
    }
}
