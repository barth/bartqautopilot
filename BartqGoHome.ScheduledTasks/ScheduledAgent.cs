﻿using System.Diagnostics;
using System.Windows;
using System.Linq;
using Microsoft.Phone.Scheduler;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using HtmlAgilityPack;
using System.Windows.Threading;
using System.Threading;
using BartqGoHome.SchedulerTaskAgent.Common;
using System.Reflection;

namespace BartqGoHome.ScheduledTasks
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        public const string LiveTileUpdaterPeriodicTaskNameString = "BartqAutopilotLiveTileUpdaterPeriodicTaskAgent";
        public static BartqGoHome.Common.Logger Log = new BartqGoHome.Common.Logger();

        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        /// Code to execute on Unhandled Exceptions
        private async static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            await IsoStorageScheduledTaskStatus.Instance.Write(e.ExceptionObject);
            Log.Log("exception bgagent", e.ExceptionObject);
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override async void OnInvoke(ScheduledTask task)
        {
            try
            {
                var settings = await ReadSettingsFromIsoStore();
                string logs = "";
                if (settings.TryGetValue("log", out logs) && logs == "yes")
                {
                    Log.AppendTags("bgagent");
                }
                else
                    Log.Clear();

                Log.Log("bgagent oninvoke trace", "Invoked by {0}. Last exit reason {1}", task.Name, task.LastExitReason);
                Log.Log("bgagent oninvoke trace", "Settings read");
                var url = await JakDojadeService.GetUrl(null, settings["tolat"], settings["tolon"], settings["city"], settings["phrase"]);
                Log.Log("bgagent oninvoke trace", "Url ackquired '{0}'", url);
                List<Route> routesOnTile = null;

                var busTiles = MyLiveTile.GetTiles().Where(tile => tile.NavigationUri.ToString().Contains("tile=Bus"));
                if (!busTiles.Any())
                    throw new Exception("Nie znaleziono kafelka komunikacji miejskiej");

                if (!string.IsNullOrWhiteSpace(url))
                {
                    string data = await GetRequest(url, settings["city"]);

                    Log.Log("bgagent oninvoke trace", "Request done");
                    Log.Log("bgagent oninvoke trace details", "Data: {0}", data);

                    HtmlDocument htmldoc = new HtmlDocument();
                    htmldoc.LoadHtml(data);
                    Log.Log("bgagent oninvoke trace", "Html document loaded");
                    List<Route> routes = ParseRoutes(htmldoc);
                    Log.Log("bgagent oninvoke trace", "Routes parsed");
                    Log.Log("bgagent oninvoke trace details", "Count: {0}", routes.Count);
                    routesOnTile = routes;
                }
                else
                {
                    Log.Log("bgagent oninvoke trace", "Url was empty");
                    routesOnTile = new List<Route>() { new Route() { Lines = "wszędzie dobrze, ale w domu najlepiej" } };
                }

                Log.Log("bgagent oninvoke trace", "Bus tiles ackquired: " + busTiles.Count());
                if (routesOnTile != null)
                {
                    EventWaitHandle Wait = new AutoResetEvent(false);
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        Log.Log("bgagent oninvoke trace", "updating tiles");
                        foreach (var tile in busTiles)
                            ActualizeTiles(routesOnTile, tile);
                        Wait.Set();
                        Log.Log("bgagent oninvoke trace", "updating tiles done");
                    }
                    );
                    Wait.WaitOne();


                }

                await IsoStorageScheduledTaskStatus.Instance.Write("ok");
                Log.Log("bgagent oninvoke trace", "work done");
            }
            catch (Exception e)
            {
                Log.Log("exception bgagent oninvoke", e);
                Task t = IsoStorageScheduledTaskStatus.Instance.Write(e);
                Task.WaitAll(t);
            }
            finally
            {
                Log.Log("bgagent oninvoke trace", "notify completion");
                NotifyComplete();
            }
        }

        private void ActualizeTiles(List<Route> routesOnTile, Microsoft.Phone.Shell.ShellTile tile)
        {
            var res = MyLiveTile.CreateFlipTileData(routesOnTile);
            if (res != null)
                tile.Update(res);
        }


        private List<Route> ParseRoutes(HtmlDocument htmldoc)
        {
            List<Route> routes = new List<Route>();
            var nodes = htmldoc.DocumentNode.SelectNodes("//table[.//td[contains(text(),'Lini')]]");
            if (nodes != null)
            {
                var nodeslist = nodes.ToList();
                foreach (var node in nodeslist)
                {
                    var route = new Route();

                    var subnode = node.SelectSingleNode("./tr[./td[contains(text(),'Lini')]]/td[2]");
                    if (subnode != null)
                        route.Lines = Stringify(subnode.InnerText);
                    else continue;

                    subnode = node.SelectSingleNode("./tr[./td[contains(text(),'Wyjazd:')]]/td[2]");
                    if (subnode != null)
                        route.StartAt = Stringify(subnode.InnerText);
                    else continue;

                    subnode = node.SelectSingleNode("./tr[./td[contains(text(),'Przyjazd:')]]/td[2]");
                    if (subnode != null)
                        route.ArriveAt = Stringify(subnode.InnerText);

                    subnode = node.SelectSingleNode("./tr[./td//*[contains(text(),'Wyjście:')]]/td[2]");
                    if (subnode != null)
                        route.LeavingAt = Stringify(subnode.InnerText);

                    routes.Add(route);
                }
            }
            return routes;
        }

        private string Stringify(string p)
        {
            return p.Replace("\n", "").Replace("&nbsp;", "").Replace("(około)", "");
        }

        private async Task<string> GetRequest(string url, string city)
        {
            WebClient wc = new CookieAwareWebClient();
            TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
            wc.DownloadStringCompleted += (s, a) =>
            {
                tcs.SetResult(a.Result);
            };
            wc.DownloadStringAsync(new Uri(url));
            await tcs.Task;
            return tcs.Task.Result;
        }


        private async Task<Dictionary<string, string>> ReadSettingsFromIsoStore()
        {
            if (!IsoStorageSettings.Instance.Exists)
                throw new Exception("Nie znaleziono pliku ustawień dla agenta");
            var settings = await IsoStorageSettings.Instance.Read();
            if (settings.Length < 5) throw new ArgumentException("Brak ustawień");
            var dict = new Dictionary<string, string>();
            dict["tolat"] = settings[0];
            dict["tolon"] = settings[1];
            dict["city"] = settings[2];
            dict["phrase"] = settings[3];
            dict["perm"] = settings[4];
            if (settings.Length > 4)
                dict["log"] = settings[5];
            if (string.IsNullOrWhiteSpace(dict["tolat"]))
            {
                throw new ArgumentOutOfRangeException("Nie ustawiono szerokości geograficznej");
            }
            if (string.IsNullOrWhiteSpace(dict["tolon"]))
            {
                throw new ArgumentOutOfRangeException("Nie ustawiono długości geograficznej");
            }
            if (string.IsNullOrWhiteSpace(dict["city"]))
            {
                throw new ArgumentOutOfRangeException("Nie ustawiono miasta");
            }
            if (string.IsNullOrWhiteSpace(dict["perm"]) || dict["perm"] != "yes")
            {
                throw new ArgumentOutOfRangeException("Brak zgody na pobieranie lokalizacji");
            }
            return dict;
        }

    }
}