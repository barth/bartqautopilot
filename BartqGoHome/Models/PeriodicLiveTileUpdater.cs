﻿#if DEBUG
#define DEBUG_AGENT
#endif
using BartqGoHome.Common;
using BartqGoHome.ScheduledTasks;
using Microsoft.Phone.Scheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BartqGoHome.Models
{
    public class PeriodicLiveTileUpdater
    {
        public static PeriodicTask LiveTileUpdaterPeriodicTask;

        public static bool StartLiveTileUpdaterPeriodicTaskAgent()
        {
            //declare the task and find the already running agent
            LiveTileUpdaterPeriodicTask = ScheduledActionService.Find(ScheduledAgent.LiveTileUpdaterPeriodicTaskNameString) as PeriodicTask;

            if (LiveTileUpdaterPeriodicTask != null)
            {
                //separate method, because we need to stop the agent when the user switches the Toggle to 'Off'
                StopLiveTileUpdaterPeriodicTaskAgent();

            }

            //generate a new background task 
            LiveTileUpdaterPeriodicTask = new PeriodicTask(ScheduledAgent.LiveTileUpdaterPeriodicTaskNameString);
            //provide a description. if not, your agent and your app may crash without even noticing you while debugging
            LiveTileUpdaterPeriodicTask.Description = "Agent w tle co 30 minut sprawdza kiedy masz transport do domu.";

            //start the agent and error handling
            try
            {
                ScheduledActionService.Add(LiveTileUpdaterPeriodicTask);
#if(DEBUG_AGENT)
                ScheduledActionService.LaunchForTest(ScheduledAgent.LiveTileUpdaterPeriodicTaskNameString, TimeSpan.FromSeconds(1));
#endif
                return true;
            }
            catch (InvalidOperationException exception)
            {
                //user deactivated or blocked the agent in phone settings/background tasks. Ask him to re-activate or unblock it
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    Task t = IsoStorageScheduledTaskStatus.Instance.WriteError("Wyłączono agentów w tle dla kafelków.");
                    Task.WaitAll(t);
                    //RadMessageBox.ShowAsync("it seems you deactivated our Background Agent for the Live Tiles. Please go to settings/background tasks to activate our app again.", "Whoops!", MessageBoxButtons.OK);
                }

                //the maximum of running background agents is reached. No further notification to the user required, as this is handled by the OS
                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    //changing the Boolean to false, because the OS does not allow any new taks
                    Task t = IsoStorageScheduledTaskStatus.Instance.WriteError("Osiągnięto maksymalną liczbę agentów w tym systemie, zwiększ budżet wywiadu.");
                    Task.WaitAll(t);
                    //isLiveTileActivated = false;
                }
            }
            catch (SchedulerServiceException e)
            {
                //if there is a problem with the service, changing the Boolean to false. 
                //feel free to inform the user about the exception and provide additional info
                //isLiveTileActivated = false;
                MessageBox.Show("Błąd z usługą agentów: " + e.Message);
            }
            return false;
        }

        public static bool StopLiveTileUpdaterPeriodicTaskAgent()
        {
            try
            {
                ScheduledActionService.Remove(ScheduledAgent.LiveTileUpdaterPeriodicTaskNameString);
                return true;
            }
            catch(Exception e) {
                MessageBox.Show("Agent urwał się ekipie wysłanej żeby go zlikwidować, miejsce przebywania nieznane." + e.Message);
            }
            return false;
        }
    }
}
