﻿using BartqGoHome.Common;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BartqGoHome.Models
{
    public class SettingsModel
    {
        private IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

        private string homePhrase = null;
        public string HomePhrase
        {
            get
            {
                return GetSetting("HomePhrase", ref homePhrase);
            }
            set
            {
                SaveSetting("HomePhrase", ref homePhrase, value);
                SaveSettingsForBackgroundAgent();
            }
        }

        internal void SaveSettingsForBackgroundAgent()
        {
            Task.Run(async () => await
                IsoStorageSettings.Instance.Write(this.homeLatitude, this.homeLongitude,
                 this.homeCity, this.homePhrase, this.geoPermission, this.logEnabled));
        }

        private string homeLatitude = null;
        public string HomeLatitude
        {
            get
            {
                return GetSetting("HomeLatitude", ref homeLatitude);
            }
            set
            {
                SaveSetting("HomeLatitude", ref homeLatitude, value);
                SaveSettingsForBackgroundAgent();
            }
        }

        private string homeLongitude = null;
        public string HomeLongitude
        {
            get
            {
                return GetSetting("HomeLongitude", ref homeLongitude);
            }
            set
            {
                SaveSetting("HomeLongitude", ref homeLongitude, value);
                SaveSettingsForBackgroundAgent();
            }
        }

        private string homeCity = null;
        public string HomeCity
        {
            get
            {
                return GetSetting("HomeCity", ref homeCity);
            }
            set
            {
                SaveSetting("HomeCity", ref homeCity, value);
                SaveSettingsForBackgroundAgent();
            }
        }

        private string backgroundAgent = null;
        public string BackgroundAgent
        {
            get
            {
                return GetSetting("BackgroundAgent", ref backgroundAgent);
            }
            set
            {
                SaveSetting("BackgroundAgent", ref backgroundAgent, value);
            }
        }

        private string logEnabled = null;
        public bool LogEnabled
        {
            get
            {
                return GetSetting("LogEnabled", ref logEnabled) == "yes";
            }
            set
            {
                SaveSetting("LogEnabled", ref logEnabled, value ? "yes" : "no");
                SaveSettingsForBackgroundAgent();
            }
        }

        private string geoPermission = null;
        public string GeoPermission
        {
            get
            {
                return GetSetting("GeoPermission", ref geoPermission);
            }
            set
            {
                SaveSetting("GeoPermission", ref geoPermission, value);
                SaveSettingsForBackgroundAgent();
            }
        }

        private void SaveSetting(string key, ref string @var, string val)
        {
            @var = val;
            settings[key] = @var;
            settings.Save();
        }

        private string GetSetting(string key, ref string @var)
        {
            if (string.IsNullOrWhiteSpace(@var))
            {
                if (settings.Contains(key))
                    @var = (string)settings[key].ToString();
                else
                    @var = string.Empty;
            }
            return @var;
        }

    }
}
