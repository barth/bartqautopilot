﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System.Threading;
using System.Globalization;
using System.Device.Location;
using Windows.Devices.Geolocation;
using BartqGoHome.Common;

namespace BartqGoHome.Pages
{
    public partial class BrowserPage : PhoneApplicationPage
    {
        public BrowserPage()
        {
            InitializeComponent();
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var last = NavigationService.BackStack.LastOrDefault();
            if (last != null)
            {
                if (last.Source.ToString().Contains("?tile"))
                    LittleWatson.CheckForPreviousException();
            }
            base.OnNavigatedFrom(e);
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            ProgressBar1.Visibility = System.Windows.Visibility.Visible;
            await ThreadPool.RunAsync(async (_) =>
            {
                try
                {
                    string url = await JakDojadeService.GetUrl(App.ViewModel.CurrentPosition, App.SettingsViewModel.Latitude,
                        App.SettingsViewModel.Longitude, App.SettingsViewModel.HomeCity.Key, App.SettingsViewModel.Address);

                    if (!string.IsNullOrWhiteSpace(url))
                    {

                        Dispatcher.BeginInvoke(() =>
                        {
                            WebBrowser.Navigate(new Uri(url));
                        });
                    }
                    else
                    {
                        Dispatcher.BeginInvoke(() =>
                        {
                            WebBrowser.NavigateToString("Wszędzie dobrze, ale w domu najlepiej. A ty jesteś nie dalej niż 100m od domu.");
                        });
                    }

                }
                catch (PlatformNotSupportedException pe)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        //WebBrowser.Visibility = System.Windows.Visibility.Collapsed;
                        WebBrowser.NavigateToString(pe.Message);
                    });
                }
                catch (NotSupportedException ne)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        //WebBrowser.Visibility = System.Windows.Visibility.Collapsed;
                        WebBrowser.NavigateToString(ne.Message);
                    });
                }
                catch (Exception ex)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        //WebBrowser.Visibility = System.Windows.Visibility.Collapsed;
                        WebBrowser.NavigateToString("Błąd: " + ex.Message);
                    });
                }
                finally
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        //WebBrowser.Visibility = System.Windows.Visibility.Collapsed;
                    });
                }
            });
        }


        private void WebBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            ProgressBar1.Visibility = System.Windows.Visibility.Collapsed;
            WebBrowser.Visibility = System.Windows.Visibility.Visible;
        }

        private void WebBrowser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            ProgressBar1.Visibility = System.Windows.Visibility.Collapsed;
            WebBrowser.Visibility = System.Windows.Visibility.Visible;
            if (e.Exception != null)
                WebBrowser.NavigateToString("Błąd: " + e.Exception.Message);
            else
                WebBrowser.NavigateToString("Wystąpił nieznany błąd. Upewnij się, że masz połączenie z internetem.");
        }

    }
}
