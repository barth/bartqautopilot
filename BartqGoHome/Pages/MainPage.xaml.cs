﻿using BartqGoHome.Common;
using BartqGoHome.ViewModels;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Maps.Services;
using Microsoft.Phone.Shell;
using Nokia.Phone.HereLaunchers;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Windows.Devices.Geolocation;

namespace BartqGoHome
{
    public partial class MainPage : PhoneApplicationPage, IDisposable
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the LongListSelector control to the sample data
            DataContext = App.ViewModel;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            InjectMapFancyTokens();

            Feedback.VisibilityChanged += Feedback_VisibilityChanged;
        }

        void Feedback_VisibilityChanged(object sender, EventArgs e)
        {
            if (ApplicationBar != null)
            {
                ApplicationBar.IsVisible = (Feedback.Visibility != Visibility.Visible);
            }
        }

        private GeoCoordinate MyCoordinate = null;

        private double _accuracy = 0.0;
        DateTime lastGeoAttempt = DateTime.Now.AddDays(-1);

        private Geolocator geolocator = null;

        private void RegisterGeolocator()
        {
            if (geolocator == null)
            {
                geolocator = new Geolocator();
                geolocator.DesiredAccuracy = PositionAccuracy.High;
                geolocator.ReportInterval = 100;
                geolocator.PositionChanged += geolocator_PositionChanged;
            }
        }

        private void UnregisterGeolocator()
        {
            if (geolocator != null)
            {
                geolocator.PositionChanged -= geolocator_PositionChanged;
                geolocator.ReportInterval = uint.MaxValue;
                geolocator = null;
            }
        }

        void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            if (args.Position != null)
            {
                _accuracy = args.Position.Coordinate.Accuracy;

                Dispatcher.BeginInvoke(() =>
                {
                    MyCoordinate = new GeoCoordinate(args.Position.Coordinate.Point.Position.Latitude, 
                        args.Position.Coordinate.Point.Position.Longitude);
                    var vm = (MainViewModel)DataContext;
                    vm.CurrentPosition = MyCoordinate;

                    Map.Center = MyCoordinate;
                    Map.ZoomLevel = 16;
                    DrawMapMarkers();

                    if ((DateTime.Now - lastGeoAttempt).TotalMinutes > 1.0)
                    {
                        ActualizeAddress();
                    }
                });

            }
        }

        private void ActualizeAddress()
        {
            GeoCoordinate geoCoordinate = MyCoordinate;
            MyReverseGeocodeQuery = new ReverseGeocodeQuery();
            MyReverseGeocodeQuery.GeoCoordinate = new GeoCoordinate(geoCoordinate.Latitude, geoCoordinate.Longitude);
            MyReverseGeocodeQuery.QueryCompleted += ReverseGeocodeQuery_QueryCompleted;
            MyReverseGeocodeQuery.QueryAsync();
        }

        private void DrawAccuracyRadius(MapLayer mapLayer)
        {
            // The ground resolution (in meters per pixel) varies depending on the level of detail
            // and the latitude at which it’s measured. It can be calculated as follows:
            double metersPerPixels = (Math.Cos(MyCoordinate.Latitude * Math.PI / 180) * 2 * Math.PI * 6378137) / (256 * Math.Pow(2, Map.ZoomLevel));
            double radius = _accuracy / metersPerPixels;

            Ellipse ellipse = new Ellipse();
            ellipse.Width = radius * 2;
            ellipse.Height = radius * 2;
            ellipse.Fill = new SolidColorBrush(Colors.Green);
            ellipse.Opacity = 0.5;
            //ellipse.Tap += GeocodeMarker_Click;

            MapOverlay overlay = new MapOverlay();
            overlay.Content = ellipse;
            overlay.GeoCoordinate = new GeoCoordinate(MyCoordinate.Latitude, MyCoordinate.Longitude);
            overlay.PositionOrigin = new Point(0.5, 0.5);
            mapLayer.Add(overlay);
        }


        private ReverseGeocodeQuery MyReverseGeocodeQuery = null;


        private void DrawMapMarkers()
        {
            Map.Layers.Clear();
            MapLayer mapLayer = new MapLayer();

            // Draw marker for current position
            if (MyCoordinate != null)
            {
                DrawAccuracyRadius(mapLayer);
                DrawMapMarker(MyCoordinate, Colors.Green, mapLayer);
            }



            Map.Layers.Add(mapLayer);
        }

        private void DrawMapMarker(GeoCoordinate coordinate, Color color, MapLayer mapLayer)
        {
            // Create a map marker
            Ellipse tag = new Ellipse();
            tag.Width = 10;
            tag.Height = 10;
            tag.Fill = new SolidColorBrush(color);

            // Enable marker to be tapped for location information
            tag.Tag = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            //tag.Tap += GeocodeMarker_Click;

            // Create a MapOverlay and add marker
            MapOverlay overlay = new MapOverlay();
            overlay.Content = tag;
            overlay.GeoCoordinate = new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
            overlay.PositionOrigin = new Point(0.5, 0.5);
            mapLayer.Add(overlay);
        }

        private void ReverseGeocodeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            if (e.Error == null)
            {
                if (e.Result.Count > 0)
                {
                    MapAddress address = e.Result[0].Information.Address;
                    var vm = (MainViewModel)DataContext;
                    string str = string.Empty;
                    string cty = str;
                    string cnt = str;

                    if (!string.IsNullOrWhiteSpace(address.Street))
                        str += ((str.Length == 0) ? string.Empty : " ") + address.Street;

                    if (!string.IsNullOrWhiteSpace(address.HouseNumber))
                        str += ((str.Length == 0) ? string.Empty : " ") + address.HouseNumber;

                    if (!string.IsNullOrWhiteSpace(address.City))
                        cty += address.City;

                    if (!string.IsNullOrWhiteSpace(address.PostalCode))
                        cty += ((cty.Length == 0) ? string.Empty : " ") + address.PostalCode;

                    if (!string.IsNullOrWhiteSpace(address.StateCode))
                        cnt += address.StateCode;

                    if (!string.IsNullOrWhiteSpace(address.Country))
                        cnt += ((cnt.Length == 0) ? string.Empty : " ") + address.Country;

                    if (!string.IsNullOrWhiteSpace(cty))
                        str += ", " + cty;

                    if (!string.IsNullOrWhiteSpace(cnt))
                        str += ", " + cnt;

                    vm.Address = str;
                }

            }
        }

        private void InjectMapFancyTokens()
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "30339542-3688-4124-9b4c-4a7e79c766ba";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "LCcKj30KXyGs4uje4pT1fg";
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("tile"))
            {
                string name = NavigationContext.QueryString["tile"];
                int mode = 0;
                if (name == "Car")
                    mode = 1;
                else if (name == "Walk")
                    mode = 2;
                LaunchAutopilot(mode);
                return;
            }
            var vm = (MainViewModel)DataContext;
            if (!vm.PermissionChecked)
            {
                PromptForGeoPermission(vm);
            }
            ApplicationBar.Mode = vm.AppbarMode;
            vm.RefreshProperty("GoButtonEnabled");
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (e.Uri.ToString().Contains("Settings"))
            {
                var vm = (MainViewModel)DataContext;
                if (vm.GeoPermission)
                {
                    RegisterGeolocator();
                }
                else
                    UnregisterGeolocator();
                vm.RefreshProperty("GoButtonEnabled");
            }
        }

        private void PromptForGeoPermission(MainViewModel vm)
        {
            if (!App.SettingsViewModel.GeoPermission)
            {
                var messagePrompt = new MessagePrompt
                {
                    Title = "Zgoda na używanie danych o Twoim położeniu",
                    Message = "Ta aplikacja do poprawnego działania wymaga dostępu do danych o Twoim położeniu. "
                        + "Szczegółowe informacje możesz znaleźć na stronie ustawień. Czy wyrażasz zgodę na wykorzystywanie tych danych?",
                    IsAppBarVisible = false,
                    IsCancelVisible = true
                };
                messagePrompt.Completed += messagePrompt_Completed;
                messagePrompt.Show();
            }
            vm.PermissionChecked = true;
        }

        void messagePrompt_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            if (e.PopUpResult == PopUpResult.Ok)
            {
                App.SettingsViewModel.GeoPermission = true;
                ((MainViewModel)DataContext).RefreshProperty("GoButtonEnabled");
                RegisterGeolocator();
            }
            else if (e.PopUpResult == PopUpResult.Cancelled)
            {
                App.SettingsViewModel.GeoPermission = false;
                ((MainViewModel)DataContext).RefreshProperty("GoButtonEnabled");
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Settings.xaml", UriKind.Relative));
        }

        private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var vm = (MainViewModel)DataContext;
            vm.SelectedMode = ((Panorama)sender).SelectedIndex;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var vm = (MainViewModel)DataContext;
            LaunchAutopilot(vm.SelectedMode);
        }

        private void LaunchAutopilot(int selectedMode)
        {
            //throw new Exception("AAA");
            var tolat = App.SettingsViewModel.Latitude;
            var tolon = App.SettingsViewModel.Longitude;
            string message = string.Empty;
            if (string.IsNullOrEmpty(tolat) || string.IsNullOrEmpty(tolon))
                message = "Zanim wystartujesz musisz ustawić współrzędne GPS domu w opcjach programu.";

            var cid = App.SettingsViewModel.HomeCity.Key;
            if (string.IsNullOrEmpty(cid))
                message += " Opcja komunikacji miejskiej wymaga także wybrania jednego ze wspieranych miast.";

            if (!string.IsNullOrEmpty(message))
                MessageBox.Show(message, "Błąd :(", MessageBoxButton.OK);
            else
            {
                string uri = string.Empty;
                switch (selectedMode)
                {
                    case 0:

                        uri = "/Pages/BrowserPage.xaml";
                        break;
                    case 1:
                        LaunchHERE(RouteMode.Car);
                        break;
                    case 2:
                        LaunchHERE(RouteMode.Pedestrian);
                        break;
                    default:
                        break;
                }
                if (!string.IsNullOrWhiteSpace(uri))
                    NavigationService.Navigate(new Uri(uri, UriKind.Relative));
            }
        }

        private void LaunchHERE(RouteMode routeMode)
        {
            var tolat = App.SettingsViewModel.Latitude;
            var tolon = App.SettingsViewModel.Longitude;
            if (routeMode == RouteMode.Car)
            {
                GuidanceDriveTask driveTo = new GuidanceDriveTask();
                driveTo.Destination = new GeoCoordinate(double.Parse(tolat), double.Parse(tolon));
                driveTo.Title = "Dom";
                driveTo.Show();
            }
            else
            {
                GuidanceWalkTask walkTo = new GuidanceWalkTask();
                walkTo.Destination = new GeoCoordinate(double.Parse(tolat), double.Parse(tolon));
                walkTo.Title = "Dom";
                walkTo.Show();
            }
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/About.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {
            ListPicker listPicker = new ListPicker()
            {
                Header = "Wybierz środek komunikacji",
                ItemsSource = new string[] { "komunikacją miejską", "samochodem", "na piechotę" },
                Margin = new Thickness(12, 42, 24, 18)
            };

            var vm = (MainViewModel)DataContext;
            listPicker.SelectedIndex = vm.SelectedMode;

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Title = "Wyciąganie kafla na pulpit",
                Caption = "Jeszcze szybszy dostęp",
                Message = "Dzięki kaflowi przypiętemu do menu głównego możesz jeszcze szybciej uruchomić wybraną funkcję autopilota.",
                Content = listPicker,
                LeftButtonContent = "anuluj",
                RightButtonContent = "utwórz",
                IsFullScreen = false
            };

            messageBox.Dismissing += (s1, e1) =>
            {
                if (listPicker.ListPickerMode == ListPickerMode.Expanded 
                    && e1.Result == CustomMessageBoxResult.RightButton)
                {
                    e1.Cancel = true;
                }
            };

            messageBox.Dismissed += (s2, e2) =>
            {
                switch (e2.Result)
                {
                    case CustomMessageBoxResult.LeftButton:
                        
                        break;
                    case CustomMessageBoxResult.RightButton:
                        int mode = listPicker.SelectedIndex;
                        MyLiveTile.CreateLiveTile(mode);
                        break;
                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;
                    default:
                        break;
                }
            };

            messageBox.Show();
        }


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}


        public void Dispose()
        {
            if (MyReverseGeocodeQuery != null)
                MyReverseGeocodeQuery.Dispose();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            LittleWatson.CheckForPreviousException();
        }
    }
}