﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.Devices.Geolocation;
using BartqGoHome.ViewModels;
using System.Threading.Tasks;
using Windows.System.Threading;
using Coding4Fun.Toolkit.Controls;
using BartqGoHome.ScheduledTasks;
using BartqGoHome.Common;
using BartqGoHome.Models;
using System.Windows.Threading;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Storage;
using Windows.ApplicationModel;
using System.IO.IsolatedStorage;

namespace BartqGoHome.Pages
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
            DataContext = App.SettingsViewModel;
        }

        private DispatcherTimer newTimer;

        async void OnTimerTick(Object sender, EventArgs args)
        {
            await GetAgentText();
        }

        private async Task GetAgentText()
        {
            string agentmsg = await IsoStorageScheduledTaskStatus.Instance.Read();
            SettingsViewModel sv = (SettingsViewModel)this.DataContext;
            sv.AgentText = Prepare(agentmsg);
        }

        private void CreateTimer()
        {
            if (newTimer == null)
            {
                newTimer = new DispatcherTimer();
                    // timer interval specified as 1 second
                    newTimer.Interval = TimeSpan.FromMinutes(1);
                    // Sub-routine OnTimerTick will be called at every 1 second
                    newTimer.Tick += OnTimerTick;
                    // starting the timer
                    newTimer.Start();
            }
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            CreateTimer();
            await GetAgentText();
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            EndTimer();
            base.OnNavigatedFrom(e);
        }

        private void EndTimer()
        {
            if (newTimer != null)
            {
                newTimer.Stop();
                newTimer = null;
            }
        }

        private string Prepare(string agentmsg)
        {
            agentmsg = agentmsg.Replace("|", " ").Replace("error","błąd");
            return agentmsg;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var vm = (SettingsViewModel)DataContext;

            await ThreadPool.RunAsync(async (_) =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    vm.ProgressVisible = Visibility.Visible;
                });
                try
                {
                    var coordinate = await GeopositionHelper.GetCurrentLocation();
                    Dispatcher.BeginInvoke(() =>
                    {
                        vm.Latitude = coordinate.Latitude.ToString();
                        vm.Longitude = coordinate.Longitude.ToString();
                        vm.SetCityCodeFromGeoPos(coordinate.Latitude, coordinate.Longitude);
                    });
                }
                catch (PlatformNotSupportedException pe)
                {
                    Dispatcher.BeginInvoke(() =>
                  {
                      MessageBox.Show(pe.Message);
                  });
                }
                catch (NotSupportedException ne)
                {
                    Dispatcher.BeginInvoke(() =>
                  {
                      MessageBox.Show(ne.Message);
                  });
                }
                catch (Exception ex)
                {
                    Dispatcher.BeginInvoke(() =>
                  {
                      MessageBox.Show("Nieznany błąd: " + ex.Message);
                  });
                }
                finally
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        vm.ProgressVisible = Visibility.Collapsed;
                    });
                }
            });
        }

        private void RoundButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = (SettingsViewModel)DataContext;
            vm.Latitude = "";
            vm.Longitude = "";
        }

        private void RoundButton_Click_1(object sender, RoutedEventArgs e)
        {
            var messagePrompt = new MessagePrompt
            {
                Title = "Informacja",
                Message = "Używani przez aplikację, zewnętrzni dostawcy map mogą mieć inne niż ty zdanie, " +
                "gdzie znajduje się podany adres. Podanie współrzędnych geograficznych jest pewniejsze. " +
                "Opcja wykorzystująca serwis jakdojade, wymaga podania jednego z obsługiwanych przez niego miast. " +
                "Opcje wykorzystujące Nokia HERE wymagają podania współrzędnych geograficznych miejsca docelowego.",
                IsAppBarVisible = true,
                IsCancelVisible = false
            };
            messagePrompt.Show();
        }

        private void RoundButton_Click_2(object sender, RoutedEventArgs e)
        {
            var messagePrompt = new MessagePrompt
            {
                Title = "Informacja",
                Message = "Aplikacja do działania potrzebuje danych o miejscu gdzie aktualnie się znajdujesz. " +
                          "Każdorazowy dostęp do tych danych odbywa się wyłącznie kiedy naciskasz odpowiednie przyciski w aplikacji " +
                          "i jest sygnalizowany przez system operacyjny charakterystyczną ikoną. " +
                          "Dane te są przekazywane do usług Nokia HERE oraz Jakdojade.pl i są przez nie przetwarzane. " +
                          "Autor nie ponosi żadnej odpowiedzialności za sposób ich przetwarzania przez dostawców wspomnianych usług. " +
                          "W dowolnej chwili możesz cofnąć swoją zgodę na pobieranie swojej pozycji, co spowoduje jednakże wyłączenie " +
                          "wszystkich funkcjonalności.",
                IsAppBarVisible = true,
                IsCancelVisible = false
            };
            messagePrompt.Show();
        }

        private void RoundButton_Click_3(object sender, RoutedEventArgs e)
        {
            var messagePrompt = new MessagePrompt
            {
                Title = "Informacja",
                Message = "Zwerbowanie agenta spowoduje, że co jakiś czas (zwykle ok pół godziny +/- 10min) sprawdzi on Twoją pozycję i " +
                          "pobierze (z jakdojade), kiedy masz następną okazję wrócić do domu. " +
                          "Natępnie umieści ją na kafelku skrótu do połączeń komunikacji miejskiej (trzeba sobie taki wyciągnąć). " +
                          "UWAGA: opcja jest stabilna, ale wciąż w fazie testów. Windows Phone nakłada dość drastyczne ograniczenia na ilość " +
                          "zarejestrowanych agentów, ich czas działania i inne sprawy. Wyłącza agentów gdy bateria jedzie na oparach, " +
                          "czasem nie pozwala wgóle ich zainstalować. W razie błędu, powinieneś zobaczyć tutaj krótki komunikat co jest nie tak.",
                IsAppBarVisible = true,
                IsCancelVisible = false
            };
            messagePrompt.Show();
        }

        private void RoundButton_Click_4(object sender, RoutedEventArgs e)
        {
            if (Logger.Present)
            {
                DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
                dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager,
                DataRequestedEventArgs>(this.ShareStorageItemsTextHandler);
                Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
            }
            else MessageBox.Show("Log file does not exists.");
        }

        private async void ShareStorageItemsTextHandler(DataTransferManager sender, DataRequestedEventArgs e)
        {
            DataRequest request = e.Request;
            // The title is mandatory
            request.Data.Properties.Title = "Bartq Autopilot log";
            request.Data.Properties.Description = "Allows to export internal log file from published app";

            // Because we are making async calls in the DataRequested event handler,
            // we need to get the deferral first.
            DataRequestDeferral deferral = request.GetDeferral();

            // Make sure we always call Complete on the deferral.

                try
                {
                    StorageFolder folder = ApplicationData.Current.LocalFolder;
                    
                    StorageFile logoFile =
                        await folder.GetFileAsync(Logger.FileName);
                    
                    List<IStorageItem> storageItems = new List<IStorageItem>();
                    storageItems.Add(logoFile);
                    request.Data.SetStorageItems(storageItems);
                }
                finally
                {
                    deferral.Complete();
                }
            
        }
    }
}