﻿using Microsoft.Phone.Shell;
using System.Device.Location;
using Windows.Devices.Geolocation;

namespace BartqGoHome.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public MainViewModel()
        {
        }

        public bool IsJakdojadeEnabled
        {
            get
            {
                return App.SettingsViewModel.GeoPermission && !string.IsNullOrWhiteSpace(App.SettingsViewModel.HomeCity.Key);
            }
        }

        public bool IsHeremapsEnabled
        {
            get
            {
                return App.SettingsViewModel.GeoPermission &&
                        !string.IsNullOrWhiteSpace(App.SettingsViewModel.Latitude) &&
                        !string.IsNullOrWhiteSpace(App.SettingsViewModel.Longitude);
            }
        }

        public bool GeoPermission {
            get
            {
               return App.SettingsViewModel.GeoPermission;
            }
        }

        private int selectedMode;
        public int SelectedMode
        {
            get
            {
                return selectedMode;
            }
            set
            {
                if (SetField(ref selectedMode, value, "SelectedMode"))
                    OnPropertyChanged("GoButtonEnabled");
            }
        }

        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                SetField(ref address, value, "Address");
            }
        }

        private GeoCoordinate currentPosition;
        public GeoCoordinate CurrentPosition
        {
            get
            {
                return currentPosition;
            }
            set
            {
                SetField(ref currentPosition, value, "CurrentPosition");
            }
        }

        public bool GoButtonEnabled
        {
            get
            {
                if (SelectedMode == 0)
                    return IsJakdojadeEnabled;
                else return IsHeremapsEnabled;
            }
        }

        public ApplicationBarMode AppbarMode
        {
            get
            {
                if (App.SettingsViewModel.GeoPermission)
                    return ApplicationBarMode.Minimized;
                else return ApplicationBarMode.Default;
            }
        }

        public bool PermissionChecked { get; set; }
    }
}