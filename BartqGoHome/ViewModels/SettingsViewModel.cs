﻿using BartqGoHome.Common;
using BartqGoHome.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BartqGoHome.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        private SettingsModel model = new SettingsModel();

        private string address;
        public string Address
        {
            get
            {
                if (address == null)
                    address = model.HomePhrase;
                return address;
            }
            set
            {
                SetField(ref address, (v) => { model.HomePhrase = value; }, value, "Address");
            }
        }

        public void SaveSettingsForAgent()
        {
            model.SaveSettingsForBackgroundAgent();
        }

        private string latittude;
        public string Latitude
        {
            get
            {
                if (latittude == null)
                    latittude = model.HomeLatitude;
                return latittude;
            }
            set
            {
                if (SetField(ref latittude, (v) => { model.HomeLatitude = value; }, value, "Latitude"))
                    OnPropertyChanged("AddressEnabled");
            }
        }


        private string agentText;
        public string AgentText
        {
            get
            {
                return agentText; 
            }
            set
            {
                SetField(ref agentText, value, "AgentText");
            }
        }

        private bool logEnabled;
        public bool LogEnabled
        {
            get
            {
                return model.LogEnabled;
            }
            set
            {
                if (value)
                {
                    if (MessageBox.Show("Prawdopodobnie nie chcesz wcale zaznaczać tej opcji - będzie Ci zjadać kilkaset Kb miejsca dziennie.",
                        "Uwaga", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                        return;
                }
                SetField(ref logEnabled, (v) => { model.LogEnabled = v; }, value, "LogEnabled");
                OnPropertyChanged("ShareButtonVisible");
                OnPropertyChanged("ShareColumnSpan");
            }
        }

        public Visibility ShareButtonVisible
        {
            get
            {
                return LogEnabled ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public int ShareColumnSpan
        {
            get
            {
                return LogEnabled ? 1 : 2;
            }
        }

        private bool? backgroundAgent;
        public bool BackgroundAgent
        {
            get
            {
                if (backgroundAgent == null)
                    backgroundAgent = model.BackgroundAgent == "yes";
                return backgroundAgent.Value;
            }
            set
            {
                string val = "yes";
                if (!value)
                    val = "no";
                if (backgroundAgent != value)
                {
                    bool set = false;
                    var task = ManageBgAgent(value);
                    if(!task.IsFaulted)
                       set=task.Result;//blocking

                    if(set)
                        SetField(ref backgroundAgent, (v) => { model.BackgroundAgent = val; }, value, "BackgroundAgent");
                }
            }
        }

        private async Task<bool> ManageBgAgent(bool value)
        {
            bool set;
            if (value)
            {
                if (!IsoStorageSettings.Instance.Exists)
                {
                    SaveSettingsForAgent();
                }
                set = PeriodicLiveTileUpdater.StartLiveTileUpdaterPeriodicTaskAgent();
                if (set)
                {
                    AgentText = "Agent pomyślnie zwerbowany.";
                    await IsoStorageScheduledTaskStatus.Instance.Write("oczekiwanie na pierwszy kontakt");
                }
                else
                {
                    AgentText = "Pozyskanie agenta zakończyło się fiaskiem.";
                    await IsoStorageScheduledTaskStatus.Instance.Write("");
                }
            }
            else
            {
                set = PeriodicLiveTileUpdater.StopLiveTileUpdaterPeriodicTaskAgent();
                if (set)
                {
                    AgentText = "Agent pomyślnie zlikwidowany.";
                    await IsoStorageScheduledTaskStatus.Instance.Write("agent zlikwidowany");
                }
                else
                {
                    AgentText = "Agent zerwał się z uwięzi.";
                    await IsoStorageScheduledTaskStatus.Instance.Write("");
                }
            }
            return set;
        }

        private bool? geoPermission;
        public bool GeoPermission
        {
            get
            {
                if (geoPermission == null)
                    geoPermission = model.GeoPermission == "yes";
                return geoPermission.Value;
            }
            set
            {
                string val = "yes";
                if (!value)
                    val = "no";
                if (SetField(ref geoPermission, (v) => { model.GeoPermission = val; }, value, "GeoPermission"))
                {
                    OnPropertyChanged("AddressEnabled");
                }
            }
        }

        private string homeCity;
        public KeyValuePair<string, string> HomeCity
        {
            get
            {
                KeyValuePair<string, string> kvphc;
                var hc = homeCity;
                if (homeCity == null)
                {
                    hc = model.HomeCity;
                }
                kvphc = cityCodes.Where(cc => cc.Key == hc).FirstOrDefault();
                homeCity = kvphc.Key;
                return kvphc;
            }
            set
            {
                SetField(ref homeCity, (v) =>
                {
                    model.HomeCity = cityCodes.Where(cc => cc.Key == value.Key)
                        .Select(cc => cc.Key)
                        .FirstOrDefault();
                }, value.Key, "HomeCity");
            }
        }

        private string longitude;
        public string Longitude
        {
            get
            {
                if (longitude == null)
                    longitude = model.HomeLongitude;
                return longitude;
            }
            set
            {
                if (SetField(ref longitude, (v) => { model.HomeLongitude = value; }, value, "Longitude"))
                    OnPropertyChanged("AddressEnabled");
            }
        }

        private bool IsGeoFilled
        {
            get
            {
                return !string.IsNullOrWhiteSpace(latittude) && !string.IsNullOrWhiteSpace(longitude);
            }
        }

        private static readonly Dictionary<string, string> cityCodes = new Dictionary<string, string>() 
        {                  
                   { string.Empty, "Nie ustawiono"},
                   { "1000" , "Poznań"},
                   { "2000" , "Wrocław"},
                   { "3000" , "Warszawa"},
                   { "4000" , "Szczecin"},
                   { "5000" , "Kraków"},
                   { "6000" , "Łódź"},
                   { "7000" , "Trójmiasto"},
                   { "8000" , "Bydgoszcz"},
                   { "8001" , "Toruń"},
                   { "9000" , "GOP"},
                   { "10000" , "Radom"},
                   { "11000" , "Białystok"}
        };

        private static readonly Dictionary<string, Tuple<double, double, double>> cityGeos = new Dictionary<string, Tuple<double, double, double>>()
        {
            { "1000" ,  new Tuple<double,double,double>(52.394668,17.011299,0.528374)},
            { "2000" , new Tuple<double,double,double>(51.113653,17.044945,0.517044)},
            { "3000" , new Tuple<double,double,double>(52.224855,21.13151,1.056747)},
            { "4000", new Tuple<double,double,double>(53.43214,14.606667,0.264187)},
            { "5000" , new Tuple<double,double,double>(50.065073,19.951859,0.528374)},
            { "6000" ,  new Tuple<double,double,double>(51.721924,19.510689,0.528374)},
            { "7000" , new Tuple<double,double,double>(54.43371,18.744736,1.056747)},
            { "8000" , new Tuple<double,double,double>(53.119066,18.054314,0.264187)},
            { "8001" ,  new Tuple<double,double,double>(53.025316,18.677101,0.528374)},
            { "9000" , new Tuple<double,double,double>(50.311708,19.074326,1.056747)},
            { "10000" , new Tuple<double,double,double>(51.39942,21.175117,0.528374)},
            { "11000" ,  new Tuple<double,double,double>(53.118963,23.174973,0.528374)}
        };

        public Dictionary<string, string> CityCodes
        {
            get
            {
                return cityCodes;
            }
        }

        public bool AddressEnabled
        {
            get
            {
                OnPropertyChanged("DeleteVisible");
                return !IsGeoFilled;
            }
        }

        public Visibility DeleteVisible
        {
            get
            {
                return IsGeoFilled ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private Visibility progressVisible = Visibility.Collapsed;
        public Visibility ProgressVisible
        {
            get
            {
                return progressVisible;
            }
            set
            {
                SetField(ref progressVisible, value, "ProgressVisible");
            }
        }

        public void SetCityCodeFromGeoPos(double lon, double lat)
        {
            foreach(var kvp in cityGeos) {
                double dlon = Math.Abs(kvp.Value.Item1 - lon);
                double dlat = Math.Abs(kvp.Value.Item2 - lat);
                if (dlon < kvp.Value.Item3 &&
                   dlat < kvp.Value.Item3)
                {
                    HomeCity = CityCodes.Where(k => k.Key == kvp.Key).FirstOrDefault();
                }
            }
        }
    }
}
